<?php get_header(); ?>

<div class="main-content py-3">
    <div class="container-fluid">
        <div class="row">
            <?php get_sidebar('wiki'); ?>
            <div class="col-lg-9">
                <div class="white_plate">
                    <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                    ?>
                    <h1 class="main_heading"><?php single_cat_title(); ?></h1>
                    <div class="desc"><?php the_archive_description(); ?></div>
		    <div class="info_plates">
<?php
global $query_string;
query_posts($query_string.'&posts_per_page=-1');
?>
                    <?php 
                        if (have_posts()) : while (have_posts()) : the_post(); 
                            printf(
                                '<a class="info_plate" href="%s"><div class="title">%s</div><div class="link">Подробнее</div></a>',
                                get_the_permalink( get_the_ID() ),
                                get_the_title( get_the_ID() )
                            );
                        endwhile; endif; 
                    ?>
                    <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
