<?php
define('WIDGET_CODE', '9wD6A1u7');
define('DEFAULT_INTEGRATION', 'umnico');
define('API_DOMAIN', 'https://console.bot-marketing.com');

define('INTEGRATION', isset($_SERVER['HTTP_X_INTEGRATION']) ? $_SERVER['HTTP_X_INTEGRATION'] : DEFAULT_INTEGRATION);
define('CATALOG_BASE_URL', API_DOMAIN.'/api/public/partners/'.INTEGRATION);

// добавляем скрипты и стили в тему
add_action('wp_enqueue_scripts', function () {
    // добавление css
    wp_enqueue_style('bot-marketing-font', 'https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap', array(), false);
    wp_enqueue_style('bot-marketing-app', 'https://console.bot-marketing.com/css/'.INTEGRATION.'/app.css', array(), false);
    //wp_enqueue_style('bot-marketing-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false);
    wp_enqueue_style('bot-marketing-fancybox', get_template_directory_uri() . '/css/jquery.fancybox.min.css', array(), false);
    wp_enqueue_style('bot-marketing-css', get_stylesheet_uri());
    wp_enqueue_style('apm-story-player', 'https://cdn.ampproject.org/amp-story-player-v0.css');

    // добавление js
    wp_enqueue_script('bot-marketing-bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), null, true);
    wp_enqueue_script('bot-marketing-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.min.js', array('jquery'), null, true);
    wp_enqueue_script('bot-marketing-custom', get_template_directory_uri() . '/js/custom.js', null, null, true);
    wp_enqueue_script('apm-story-player', 'https://cdn.ampproject.org/amp-story-player-v0.js');

    /*
	// регистрация другой версии jQuery
	if( !is_admin()){
		wp_deregister_script('jquery');
		wp_register_script('jquery', ("http://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js "), false, '3.5.1');
		wp_enqueue_script('jquery');
    }
    */
});

/**
 * Перезаписываем url сайта, если передан X-Base-Path
 *
 * @param  mixed $val
 * @return void
 */
function replace_siteurl_xbase( /*$config_wp_siteurl*/ ) {
    ini_set("user_agent","Mozilla");
    $headers = get_headers( $_SERVER['HTTP_HOST'], 1 );
    //echo $_SERVER['HTTP_HOST'];
    if( !$headers ) {
        //echo $_SERVER['HTTP_HOST'];
    }
    print_r( $headers );
    if( in_array( 'X-Base-Path', $headers  ) ) {
        echo 'X-Base-Path';
    }
    //return 'http://'.$_SERVER['HTTP_HOST'];
}
//add_action('init', 'replace_siteurl_xbase');
//add_filter('option_siteurl', 'replace_siteurl');
//add_filter('option_home', 'replace_siteurl');

function replace_media_links($content) {
	return str_replace('https://catalog-umnico.bot-marketing.com', site_url(), $content);
}

if (defined('HIDE_HEADER')) add_filter('the_content', 'replace_media_links');

// подключаем обработку meta title
add_theme_support('title-tag');

// Установка меню
if (function_exists('register_nav_menu')) {
    register_nav_menu('top-menu-left', 'Меню на черном, слева');
    register_nav_menu('top-menu-right', 'Меню на черном, справа');
    register_nav_menu('second-menu', 'Меню под шапкой');
}

// Disable WordPress Lazy Load
add_filter( 'wp_lazy_loading_enabled', '__return_false' );

add_filter( 'wpseo_breadcrumb_single_link' ,'wpseo_remove_breadcrumb_link', 10 ,2);
function wpseo_remove_breadcrumb_link( $link_output , $link ){
    $text_to_remove = 'База знаний';

    if( $link['text'] == $text_to_remove ) {
        $link['url'] = site_url().'/wiki/';
        $link_output = '<span><a href="'.$link['url'].'">'.$link['text'].'</a>';
    }

    if (defined('HIDE_HEADER')) {
	    $link_output = replace_media_links($link_output);
    }

    return $link_output;
}

class Header_Menu extends Walker_Nav_Menu {
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output
	 * @param object $item Объект элемента меню, подробнее ниже.
	 * @param int $depth Уровень вложенности элемента меню.
	 * @param object $args Параметры функции wp_nav_menu
	 */
	function start_el(&$output, $item, $depth = 0, $args = NULL, $id = 0) {
		global $wp_query;
		/*
		 * Некоторые из параметров объекта $item
		 * ID - ID самого элемента меню, а не объекта на который он ссылается
		 * menu_item_parent - ID родительского элемента меню
		 * classes - массив классов элемента меню
		 * post_date - дата добавления
		 * post_modified - дата последнего изменения
		 * post_author - ID пользователя, добавившего этот элемент меню
		 * title - заголовок элемента меню
		 * url - ссылка
		 * attr_title - HTML-атрибут title ссылки
		 * xfn - атрибут rel
		 * target - атрибут target
		 * current - равен 1, если является текущим элементом
		 * current_item_ancestor - равен 1, если текущим (открытым на сайте) является вложенный элемент данного
		 * current_item_parent - равен 1, если текущим (открытым на сайте) является родительский элемент данного
		 * menu_order - порядок в меню
		 * object_id - ID объекта меню
		 * type - тип объекта меню (таксономия, пост, произвольно)
		 * object - какая это таксономия / какой тип поста (page /category / post_tag и т д)
		 * type_label - название данного типа с локализацией (Рубрика, Страница)
		 * post_parent - ID родительского поста / категории
		 * post_title - заголовок, который был у поста, когда он был добавлен в меню
		 * post_name - ярлык, который был у поста при его добавлении в меню
		 */
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		/*
		 * Генерируем строку с CSS-классами элемента меню
		 */
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		// функция join превращает массив в строку
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="nav-item ' . esc_attr( $class_names ) . '"';

		/*
		 * Генерируем ID элемента
		 */
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		/*
		 * Генерируем элемент меню
		 */
		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		// атрибуты элемента, title="", rel="", target="" и href=""
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		// ссылка и околоссылочный текст
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .' class="nav-link">';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

 		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

function bot_acf_options_page_settings( $settings )
{
	$settings['title'] = 'Настройки сайта';
    $settings['menu'] = 'Настройки сайта';
	return $settings;
}
add_filter('acf/options_page/settings', 'bot_acf_options_page_settings');

// добавляем класс selected для активного пункта меню
add_filter('nav_menu_css_class', function ($classes, $item) {
	if (in_array('current-menu-item', $classes)) {
		$classes[] = 'active';
	}
	return $classes;
}, 10, 2);

// Подключаем миниатюры
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
}

// Задаем названия и размеры миниатюр
if (function_exists('add_image_size')) {
	add_image_size('solution-screen', 274, 487, true);
}

add_filter( 'wp_image_editors', 'select_wp_image_editors' );
function select_wp_image_editors( $editors ) {
    return array( 'WP_Image_Editor_Imagick' );
}

// Отключаем resize для определенных типов файлов
function disable_upload_sizes( $sizes, $metadata ) {

    // Get filetype data.
    $filetype = wp_check_filetype( $metadata['file'] );

    // Check if is gif.
    if($filetype['type'] == 'image/gif') {
        // Unset sizes if file is gif.
        $sizes = array();
    }

    // Return sizes you want to create from image (None if image is gif.)
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'disable_upload_sizes', 10, 2);

// Снимаем ограничение на большие разрешения
add_filter( 'big_image_size_threshold', '__return_false' );

// Регистрируем типы записей
add_action('init', function () {
	register_post_type('solution', array(
		'public' => true,
        'has_archive' => false,
        'show_ui' => true,
        'show_in_menu' => true,
		'menu_icon' => 'dashicons-admin-network',
		'labels' => array(
			'name' => 'Решения',
			'singular_name' => 'Решение',
			'search_items' => 'Найти решение',
			'all_items' => 'Решения',
			'add_new' => 'Добавить решение',
			'edit_item' => 'Редактировать',
			'update_item' => 'Обновить',
			'view_item' => 'Посмотреть решение',
			'new_item' => 'Добавить решение',
			'menu_name' => 'Решения'
		),
		'rewrite' => array(
			'slug' => 'solution',
			'with_front' => false
		),
		'supports' => array('title', 'editor', 'thumbnail')
    ));

    register_post_type('tagline', array(
		'public' => true,
        'has_archive' => false,
        'show_ui' => true,
        'show_in_menu' => true,
		'menu_icon' => 'dashicons-images-alt2',
		'labels' => array(
			'name' => 'Группы',
			'singular_name' => 'Группа',
			'search_items' => 'Найти',
			'all_items' => 'Группы',
			'add_new' => 'Добавить',
			'edit_item' => 'Редактировать',
			'update_item' => 'Обновить',
			'view_item' => 'Посмотреть',
			'new_item' => 'Добавить',
			'menu_name' => 'Группы'
		),
		'rewrite' => array(
			'slug' => 'tagline',
			'with_front' => false
		),
		'supports' => array('title', 'editor', 'thumbnail')
	));
});

add_filter( 'pre_get_posts', 'bot_add_custom_post_types_to_query' );
function bot_add_custom_post_types_to_query( $query ) {
	if(
		is_archive() &&
		$query->is_main_query() &&
		empty( $query->query_vars['suppress_filters'] )
	) {
		$query->set( 'post_type', array(
			'post',
            'solution',
            'tagline'
		) );
	}
}

// правила rewrite
add_action('init', function () {
    // Правила перезаписи
	add_rewrite_rule(
        '^(solution)/?([^/]*)_([0-9]{1,})/?$',
        'index.php?pagename=$matches[1]&slug=$matches[2]&solution_id=$matches[3]',
        'top'
    );
    add_rewrite_rule(
        '^(tagline)/?([^/]*)_([0-9]{1,})/?$',
        'index.php?pagename=$matches[1]&slug=$matches[2]&tag_id=$matches[3]',
        'top'
    );

	// скажем WP, что есть новые параметры запроса
	add_filter('query_vars', function ($vars) {
        $vars[] = 'slug';
        $vars[] = 'solution_id';
        $vars[] = 'tag_id';
        $vars[] = 'pagination';
		return $vars;
	});
});

add_action('login_enqueue_scripts', function () {
	wp_enqueue_style('bot-marketing-login', get_template_directory_uri() . '/style.css');
});

/**
 * Получаем список тегов
 *
 * @return array
 */
function get_taglist() {
    return json_decode( file_get_contents(CATALOG_BASE_URL.'/catalog/tags'), true );
}

/**
 * Получаем список решений
 *
 * @return array
 */
function get_solutionslist( $perPage = 6, $page = 1 ) {
    $data = file_get_contents(CATALOG_BASE_URL."/catalog?perPage={$perPage}&page={$page}");

    return json_decode( $data, true );
}

/**
 * Получает данные группы по tag_id
 *
 * @param  integer $tag_id
 * @return array
 */
function get_tagline_by_id( $tag_id = 0, $perPage = 6, $page = 1 ) {
    $data = file_get_contents(CATALOG_BASE_URL."/catalog?tags[0][id]={$tag_id}&perPage={$perPage}&page={$page}");
    return json_decode( $data, true );
}

/**
 * Получает данные решения по id
 *
 * @param  integer $solition_id
 * @return array
 */
function get_solution_by_id( $solition_id = 0 ) {
    $data = file_get_contents(CATALOG_BASE_URL."/catalog/{$solition_id}");
    $data = json_decode( $data, true );
    return $data;
}

/**
 * Принудительно записывает контент в мета-поле записи
 *
 * @param  mixed $post_ID
 * @param  mixed $post
 * @param  mixed $update
 * @return void
 */
function force_save_post_content( $post_ID, $post, $update ) {
    update_post_meta( $post_ID, 'forced_post_content', $post->post_content );
}
add_action( 'save_post', 'force_save_post_content', 10, 3 );

/**
 * Принудительно получает контент из мета-поля записи
 *
 * @param  integer $post_id
 * @return void
 */
function force_get_post_content( $post_id ) {
    global $post;
    if( empty( get_the_content( $post_id ) ) ) {
        $post->post_content = get_post_meta( $post_id, 'forced_post_content', true );
    }
}
add_action( 'post_action_edit', 'force_get_post_content', 10 );

/**
 * Получает решения по строке поиска
 *
 * @param  string $string
 * @return array
 */
function search_solutions( $string ) {
    $data = file_get_contents(CATALOG_BASE_URL."/catalog?search=".rawurlencode($string));
    $data = json_decode( $data, true );
    return $data['items'];
}

function bot_get_taglines( $atts ) {
    $atts = shortcode_atts(
        array(
            'id' => '14',
            'perpage' => '6',
            'page' => '1'
        ),
        $atts
    );

    $url_string = '';
    $tags = explode(',', $atts['id']);
    foreach( $tags as $key => $value ) {
        $url_string .= "tags[{$key}][id]={$value}&";
    }

    $url_string .= "perPage={$atts['perpage']}&page={$atts['page']}";

    $data = file_get_contents(CATALOG_BASE_URL."/catalog?{$url_string}");
    $data = json_decode( $data, true );
    ob_start();
    foreach( $data['items'] as $item )  {
        include( locate_template('template-part-solutions.php') );
    }
    $content = ob_get_clean();
    return $content;
}
add_shortcode('bot_get_taglines', 'bot_get_taglines');



// автоверсии CSS/JS при изменении их версии сохранения
add_filter('style_loader_src', 'autover_version_filter');
add_filter('script_loader_src', 'autover_version_filter');
function autover_version_filter($src) {
	$url_parts = wp_parse_url($src);

	$extension = pathinfo($url_parts['path'], PATHINFO_EXTENSION);
	if (!$extension || !in_array($extension, ['css', 'js'])) {
		return $src;
	}

	if (defined('AUTOVER_DISABLE_' . strtoupper($extension))) {
		return $src;
	}

	$file_path = rtrim(ABSPATH, '/') . urldecode($url_parts['path']);
	if (!is_file($file_path)) {
		return $src;
	}

	$timestamp_version = filemtime($file_path) ?: filemtime(utf8_decode($file_path));
	if (!$timestamp_version) {
		return $src;
	}

	if (!isset($url_parts['query'])) {
		$url_parts['query'] = '';
	}

	$query = [];
	parse_str($url_parts['query'], $query);
	unset($query['v']);
	unset($query['ver']);
	$query['ver'] = "$timestamp_version";
	$url_parts['query'] = build_query($query);

	return autover_build_url($url_parts);
}

function autover_build_url(array $parts) {
	return (isset($parts['scheme']) ? "{$parts['scheme']}:" : '') .
		((isset($parts['user']) || isset($parts['host'])) ? '//' : '') .
		(isset($parts['user']) ? "{$parts['user']}" : '') .
		(isset($parts['pass']) ? ":{$parts['pass']}" : '') .
		(isset($parts['user']) ? '@' : '') .
		(isset($parts['host']) ? "{$parts['host']}" : '') .
		(isset($parts['port']) ? ":{$parts['port']}" : '') .
		(isset($parts['path']) ? "{$parts['path']}" : '') .
		(isset($parts['query']) ? "?{$parts['query']}" : '') .
		(isset($parts['fragment']) ? "#{$parts['fragment']}" : '');
}

/* Шоткоды */
function wiki_post_intro( $atts, $content = '' ) {
	return '<div class="intro">'.$content.'</div>';
}
add_shortcode('intro', 'wiki_post_intro');

function wiki_post_alert( $atts, $content = '' ) {
	return '<span class="alert">'.$content.'</span>';
}
add_shortcode('alert', 'wiki_post_alert');

function wiki_post_code( $atts, $content = '' ) {
	return '<pre><code>'.htmlentities($content).'</code></pre>';
}
add_shortcode('code', 'wiki_post_code');

function story_player($attrs, $content = '') {
	return '<amp-story-player style="width: 360px; height: 600px;">'.$content.'</amp-story-player>';
}
add_shortcode('story-player', 'story_player');
