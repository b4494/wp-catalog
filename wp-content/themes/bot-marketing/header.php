<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
    <?php wp_head(); ?>
</head>
<body>
<div class="d-flex flex-column justify-content-between">
    <div id="app">
<?php /*if (!defined('HIDE_HEADER')):*/ ?>
        <nav id="main-nav" class="navbar navbar-expand-lg navbar-<?php echo INTEGRATION; ?>">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
                <?php
                    wp_nav_menu(array(
                        'theme_location'  => 'top-menu-left',
                        'container'       => '',
                        'menu_class'      => 'navbar-nav mr-auto main-nav',
                        'walker'          => new Header_Menu()
                    ));
                ?>
                <?php
                    wp_nav_menu(array(
                        'theme_location'  => 'top-menu-right',
                        'container'       => '',
                        'menu_class'      => 'navbar-nav',
                        'walker'          => new Header_Menu()
                    ));
                ?>
            </div>
        </nav>
        <!--<nav id="second-nav" class="shadow">
            <div class="container-fluid">
                <div class="col-12">
                <?php
                    wp_nav_menu(array(
                        'theme_location'  => 'second-menu',
                        'container'       => '',
                        'menu_class'      => ''
                    ));
                ?>
                </div>
            </div>
	</nav>-->
<?php /*endif;*/ ?>
        <div class="container-fluid">

