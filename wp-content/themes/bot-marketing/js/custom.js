jQuery(document).ready(function ($) {
    $(document).on('click', '.btn-demo', function() {
        var name = $(this).data('name'),
            hash = $(this).data('hash'),
            whatsapp = $(this).data('whatsapp'),
            telegram = $(this).data('telegram'),
            viber = $(this).data('viber');

        let $whatsapp = $('#whatsapp');
        $whatsapp.css('display', 'none');
        let $telegram = $('#telegram');
        $telegram.css('display', 'none');
        let $viber = $('#viber');
        $viber.css('display', 'none');

        if( whatsapp.length ) {
            $whatsapp.css('display', 'block');
            $whatsapp.attr('href', $whatsapp.attr('href') + name + ' ({code})');
            $whatsapp.attr('data-hash', hash);
        }

        if( telegram.length ) {
            $telegram.css('display', 'block');
            $telegram.attr('data-hash', hash);
        }

        //if( viber.length ) {
        //    $viber.css('display', 'block');
        //    $viber.attr('href', 'viber://pa?chatURI=botmarketingbusiness&text=Хочу демо ' + name + ' (restart_wBgN4LE4_1_' + hash + '_null)');
        //    $viber.attr('data-hash', hash);
        //}

        $.fancybox.open({
            src  : '#demo_solution'
        });

        document.addEventListener( 'wpcf7submit', function( event ) {
            ym(48120641, 'reachGoal', 'form_sent');
        }, false );
    });

    $(document).on( 'click', '.cat-sidebar-section:not(.open) > a', function(e){
        e.preventDefault();
        $('.cat-sidebar-section').removeClass('open');
        $(this).closest('.cat-sidebar-section').addClass('open');
    });
});