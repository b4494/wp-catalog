<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="main-content py-3">
    <div class="main-container">
        <div class="jumbotron shadow-lg">
            <div class="row">
                <div class="col-lg-7 overflow-hidden pb-1">
                    <div class="d-flex flex-column h-100">
                        <h1 class="mb-5"><?php the_title(); ?></h1>
                        <div class="font-weight-normal mb-5 h5"><?php the_content(); ?></div>
                    </div>
                </div>
                <div class="col-lg-5 d-none d-lg-flex justify-content-center">
                    <div class="solution-demo-img"><?php if ( has_post_thumbnail() ) the_post_thumbnail('solution-screen'); ?> </div>
                </div>
            </div>
        </div>
        <?php if ( $page_shortcode = get_field('page_shortcode') ) { ?>
        <div class="py-5">
            <div class="solution-list-container" id="solutions">
                <div class="col-lg-12">
                    <div class="row solution-list">
                        <?php echo do_shortcode( $page_shortcode ); ?>
                    </div>
                </div>        
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>