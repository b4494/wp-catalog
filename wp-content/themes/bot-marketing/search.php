<?php get_header('catalog'); ?>
<div class="main-content py-3">
        <div class="solution-list-container" id="solutions">
            <div class="row mb-xl-3">
                <div class="col mr-auto">
                    <h2>Вы искали "<?php echo get_search_query(); ?>"</h2>
                </div>
                <div class="col-lg-4 col-sm-5">
                    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="text" placeholder="Поиск по названию" name="s" class="form-control mb-3 mr-sm-3">
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2">
                    <div class="card mb-3 shadow sticky-top tags-menu">
                        <div class="card-body pb-0">
                            <?php
                                // отбираем и раскладываем теги по категориям
                                $business = $integration = $messenger = $other = array();
                                $tags = get_taglist();
                                foreach( $tags as $tag ) {
                                    switch ( $tag['ct'] ) {
                                        case 'business':
                                            array_push( $business, $tag );
                                            break;
                                        case 'integration':
                                            array_push( $integration, $tag );
                                            break;
                                        case 'messenger':
                                            array_push( $messenger, $tag );
                                            break;
                                        default:
                                            array_push( $other, $tag );
                                    }
                                }
                            ?>
                            <div class="h6 mb-2">Сфера бизнеса</div>
                            <div class="mb-3">
                                <?php foreach( $business as $business_tag ) {
                                        printf(
                                            '<a href="/tagline/%s">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $business_tag['lb'] ) ).'_'.$business_tag['id'],
                                            $business_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Интеграции</div>
                            <div class="mb-3">
                                <?php foreach( $integration as $integration_tag ) {
                                        printf(
                                            '<a href="/tagline/%s">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $integration_tag['lb'] ) ).'_'.$integration_tag['id'],
                                            $integration_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Мессенджеры</div>
                            <div class="mb-3">
                                <?php foreach( $messenger as $messenger_tag ) {
                                        printf(
                                            '<a href="/tagline/%s">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $messenger_tag['lb'] ) ).'_'.$messenger_tag['id'],
                                            $messenger_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Метки</div>
                            <div class="mb-3">
                                <?php foreach( $other as $other_tag ) {
                                        printf(
                                            '<a href="/tagline/%s">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $other_tag['lb'] ) ).'_'.$other_tag['id'],
                                            $other_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="row solution-list">
                    <?php $search_items = search_solutions(get_search_query()); ?>
                    <?php if(!empty($search_items)): ?>
                      <?php foreach( $search_items as $item ): ?>
                        <?php include(locate_template('template-part-solutions.php')); ?>
                      <?php endforeach; ?>
                    <?php else: ?>
                        <div class="col-12">
                            <div class="card card-body"><p>Решения по запросу "<?php echo get_search_query(); ?>" не найдены. Пожалуйста, повторите поиск по другому слову или воспользутесь меню каталога. </p></div>
                        </div>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
    </div>
</div>
<?php include( locate_template('template-part-modal.php') ); ?>
<?php get_footer(); ?>
