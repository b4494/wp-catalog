<?php 
    global $post;
    $post_id = $post->ID;
    $post_terms = get_the_terms( $post->ID, 'category' );
    $post_parents = array();
    if( $post_terms ) {
        foreach( $post_terms as $post_term ) {
            $post_parents[] = $post_term->term_id;
        }
    }
?>
<div class="col-lg-3">
    <div class="wiki_sidebar">
        <div class="pb-0">
            <div class="sidebar_heading">Навигация</div>
            <?php 
                $categories = get_terms(array(
                    'parent' => 0, 
                    'hide_empty' => 1, 
                    'hierarchical' => 1, 
                    'taxonomy' => 'category', 
                    'pad_counts' => false,
                    'orderby' => 'term_id'
                ));
                foreach ($categories as $category) {
                    $cat_switcher = in_array( $category->term_id, $post_parents ) ? true : false;
                    $category_posts = array();
            ?>
                <div class="cat-sidebar-section <?php if( $cat_switcher ) echo 'open'; ?>">
                    <a class="cat-link" href="<?php echo esc_url(get_category_link($category->term_id)); ?>"><?php echo esc_html($category->name); ?></a>
                    <?php
                    $cat_post = new WP_Query(array(
                        'posts_per_page' => -1,
                        'tax_query' => array(
                            'relation' => 'AND',
                            array(
                                'taxonomy' => 'category',
                                'field' => 'id',
                                'terms' => array($category->term_id),
                            )
                        ),
                        'order' => 'ASC'
                    ));
                    if ($cat_post->have_posts()) : ?>
                        <div class="entries">
                            <ul>
                                <?php while ($cat_post->have_posts()) : $cat_post->the_post(); if( in_array( get_the_ID(), $category_posts ) ) continue; ?>
                                    <li>
                                        <a href="<?php the_permalink(); ?>" class="<?php /* if( $post_id == get_the_ID() ) echo 'bold'; */ ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    <?php endif; wp_reset_postdata(); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>