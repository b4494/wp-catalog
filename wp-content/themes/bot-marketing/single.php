<?php 
get_header(); 
global $post;
?>
<div class="main-content py-3">
    <div class="container-fluid">
        <div class="row">
            <?php get_sidebar('wiki'); ?>
            <div class="col-lg-9">
                <div class="post_content white_plate">
                    <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                    ?>
                    <h1 class="post_heading"><?php echo get_the_title( $post->ID ); ?></h1>
                    <?php 
                        $get_the_content = get_the_content( $post->ID );
                        if( !empty( $get_the_content ) ) {
                            echo apply_filters( 'the_content', do_shortcode( $get_the_content ) );
                        } else {
                            $get_the_content = get_post_meta( $post->ID, 'forced_post_content', true );
                            echo do_shortcode( $get_the_content );
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer('wiki'); ?>
