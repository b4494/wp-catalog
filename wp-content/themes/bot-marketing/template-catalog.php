<?php
/* Template name: Каталог */
get_header('catalog2');
?>

<div class="main-content d-flex flex-column justify-content-center align-items-center">

<div class = "row">

<div class = "col-lg-1">

</div>

<div class = "col-lg-2 order-lg-0 col-sm-12 col-md-4 order-md-0 col-12 order-3">


                    <div class="card p-2 shadow overflow-auto">
                        	
						
						<div class="card-body">
                         
                       <div class = "row">
						 <?php
                                // отбираем и раскладываем теги по категориям
                                $business = $integration = $messenger = $other = array();
                                $tags = get_taglist();
                                foreach( $tags as $tag ) {
                                    switch ( $tag['ct'] ) {
                                        case 'business':
                                            array_push( $business, $tag );
                                            break;
                                        case 'integration':
                                            array_push( $integration, $tag );
                                            break;
                                        case 'messenger':
                                            array_push( $messenger, $tag );
                                            break;
                                        default:
                                            array_push( $other, $tag );
                                    }
                                }
                            ?>
                           

         <div class="col-lg-12 col-sm-6 col-md-12 col-6">
						   <div class="h6 mb-2 fs-7 text-secondary fw-bold "><svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="12" height="12" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class="pe-1 mb-1"><g>
<g>
	<g>
		<path d="M256,0C114.837,0,0,114.837,0,256s114.837,256,256,256s256-114.837,256-256S397.163,0,256,0z" fill="#EF3311" data-original="#000000" class=""></path>
	</g>
</g>
</svg>Сфера бизнеса</div>
                            <div class="ms-2 mb-3">
                                <?php foreach( $business as $business_tag ) {
                                        printf(
                                            '<a class="fs-7 text-secondary list-group-item list-group-item-action list-group-item-light mb-n4 p-1" href="'.home_url( '/tagline/%s').'">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $business_tag['lb'] ) ).'_'.$business_tag['id'],
                                            $business_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
							
		</div>					
		<div class="col-lg-12 col-sm-6 col-md-12 col-6">					
                            <div class="h6 mb-2 fs-7 text-secondary fw-bold"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="12" height="12" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class="pe-1 mb-1"><g>
<g>
	<g>
		<path d="M256,0C114.837,0,0,114.837,0,256s114.837,256,256,256s256-114.837,256-256S397.163,0,256,0z" fill="#1155EF" data-original="#000000" class=""></path>
	</g>
</g>

 
</svg>Интеграции</div>
                            <div class="mb-3 ms-2 fs-7 text-secondary">
                                <?php foreach( $integration as $integration_tag ) {
                                        printf(
                                            '<a class="fs-7 text-secondary list-group-item list-group-item-action list-group-item-light mb-n4 p-1" href="'.home_url('/tagline/%s').'">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $integration_tag['lb'] ) ).'_'.$integration_tag['id'],
                                            $integration_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
							
							
</div>							
	<div class="col-lg-12 col-sm-6 col-md-12 col-6">						
                            <div class="h6 mb-2 fs-7 text-secondary fw-bold"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="12" height="12" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class="pe-1 mb-1"><g>
<g>
	<g>
		<path d="M256,0C114.837,0,0,114.837,0,256s114.837,256,256,256s256-114.837,256-256S397.163,0,256,0z" fill="#263804" data-original="#000000" class=""></path>
	</g>
</g>
</svg>Метки</div>
                            <div class="mb-3 ms-2 fs-7 text-secondary">
                                <?php foreach( $other as $other_tag ) {
                                        printf(
                                            '<a class="fs-7 text-secondary list-group-item list-group-item-action list-group-item-light mb-n4 p-1" href="'.home_url('/tagline/%s').'">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $other_tag['lb'] ) ).'_'.$other_tag['id'],
                                            $other_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
					</div>		
							
						</div>	
                        </div>
                    </div>
   

</div>



<div class = "col-lg-8 col-sm-12 col-md-8 col-12">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="solution-list-container ms-3" id="solutions">
                 <div class="col-lg-8 mt-lg-0 col-sm-12 col-md-8 mt-md-0 col-11 mt-3">
                <h4 class="text-dark fs-4">Найдите наиболее подходящее для вас решение</h4>
                
                    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="text" placeholder="Поиск по названию" name="s" id="s" class="form-control mb-3 mr-sm-3">
                    </form>
					
				<div class="search-btn mt-1 ms-1 d-inline-flex">

				<button type="button" class="btn btn-outline-danger me-3 btn-sm">Все каналы</button>

				<button type="button" class="btn btn-outline-danger me-3 btn-sm">Telegram</button>

				<button type="button" class="btn btn-outline-danger me-3 btn-sm">WhatsApp</button>

				<button type="button" class="btn btn-outline-danger me-3 btn-sm">Avito</button>
              </div>
				
                </div>
 
        
                <div class="col-lg-12 col-sm-12 col-md-8 col-12 d-flex flex-column ms-1">
                    <div class="row solution-list mt-4 pe-3">
                    <?php
                        $current_page = get_query_var( 'pagination' );
                        $solutions_per_page = 100;
                        $solutions = get_solutionslist( $solutions_per_page, $current_page );
                        foreach( $solutions['items'] as $item ) {
                            include( locate_template('template-part-solutions.php') );
                        }
                    ?>
                    </div>
                    <!---->
                    <nav>
                        <div class="pagination mb-0 justify-content-center">
                            <?php
                            echo paginate_links( array(
                                'base'         => @add_query_arg('pagination', '%#%'),
                                'format'       => '?pagination=%#%',
                                'total'        => $solutions['totalPages'],
                                'current'      => max( 1, $solutions['currentPage'] ),
                                'show_all'     => true,
                                'end_size'     => 1,
                                'mid_size'     => 2,
                                'prev_next'    => false,
                                'prev_text'    => __('«'),
                                'next_text'    => __('»'),
                                'type'         => 'plain',
                                'add_args'     => false,
                                'add_fragment' => '',
                                'before_page_number' => '',
                                'after_page_number'  => ''
                            ));
                            ?>
                        </div>
                    </nav>
        
            </div>
        </div>
<?php endwhile; endif; ?>
<?php include( locate_template('template-part-modal.php') ); ?>

</div>



</div>



</div>





