<?php 
/* Template name: Главная страница */ 
get_header(); 
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="main-content py-3">
    <div class="white-wrap mt-n3 pt-5">
        <div class="container main-container">
            <div class="row align-items-center">
                <div class="col-md-8 col-lg-5 mb-4">
                    <div class="h1 mb-4">Готовые боты для работы в чатах</div>
                    <p class="text-muted mb-4">
                        С помощью наших чат-ботов вы перестанете терять чаты и увеличите продажи своего
                        магазина. Для установки бота не потребуется специальных навыков, достаточно лишь несколько секунд и пара
                        кликов.
                    </p> 
                    <a href="#solutions" class="btn px-5 btn-primary btn-lg">Перейти к решениям</a>
                </div>
                <div class="col-md-4 col-lg-7 text-center mb-4"><img src="<?php bloginfo('template_url'); ?>/images/solutions-primary.svg" class="img-fluid" width="392" height="571"></div>
            </div>
        </div>
        <div class="pb-5"></div>
    </div>
    <div class="py-5">
        <div class="solution-list-container" id="solutions">
            <div class="row mb-xl-3">
                <div class="col mr-auto">
                    <h2>Каталог решений</h2>
                </div>
                <div class="col-lg-4 col-sm-5">
                    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="text" placeholder="Поиск по названию" name="s" class="form-control mb-3 mr-sm-3">
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2">
                    <div class="card mb-3 shadow sticky-top tags-menu">
                        <div class="card-body pb-0">
                            <div class="d-flex">
                                <div class="h4 mb-3 mr-auto">Группы решений</div>
                            </div>
                            <?php 
                                // отбираем и раскладываем теги по категориям
                                $business = $integration = $messenger = $other = array();
                                $tags = get_taglist(); 
                                foreach( $tags as $tag ) {
                                    switch ( $tag['ct'] ) {
                                        case 'business':
                                            array_push( $business, $tag );
                                            break;
                                        case 'integration':
                                            array_push( $integration, $tag );
                                            break;
                                        case 'messenger':
                                            array_push( $messenger, $tag );
                                            break;
                                        default:
                                            array_push( $other, $tag );
                                    }
                                }
                            ?>
                            <div class="h6 mb-2">Сфера бизнеса</div>
                            <div class="mb-3">
                                <?php foreach( $business as $business_tag ) {
                                        printf(
                                            '<a href="/tagline/%s">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $business_tag['lb'] ) ).'_'.$business_tag['id'],
                                            $business_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Интеграции</div>
                            <div class="mb-3">
                                <?php foreach( $integration as $integration_tag ) {
                                        printf(
                                            '<a href="/tagline/%s">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $integration_tag['lb'] ) ).'_'.$integration_tag['id'],
                                            $integration_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Мессенджеры</div>
                            <div class="mb-3">
                                <?php foreach( $messenger as $messenger_tag ) {
                                        printf(
                                            '<a href="/tagline/%s">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $messenger_tag['lb'] ) ).'_'.$messenger_tag['id'],
                                            $messenger_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Метки</div>
                            <div class="mb-3">
                                <?php foreach( $other as $other_tag ) {
                                        printf(
                                            '<a href="/tagline/%s">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $other_tag['lb'] ) ).'_'.$other_tag['id'],
                                            $other_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="row solution-list">
                    <?php 
                        $solutions = get_solutionslist();
                        foreach( $solutions['items'] as $item ) {
                            include( locate_template('template-part-solutions.php') ); 
                        } 
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pb-5"></div>
    <div class="container main-container">
        <div id="advantages" class="card shadow-lg mb-5 main-card">
            <div class="card-body p-5">
                <div class="p-lg-5">
                    <div class="row">
                        <div class="col-md-8 col-lg-5">
                            <div class="h1 mb-4">Преимущества наших решений</div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-4 col-lg-6 text-center order-md-last mb-5"><img src="<?php bloginfo('template_url'); ?>/images/startup-svg.svg" class="img-fluid" width="312" height="320"></div>
                        <div class="col-md-8 col-lg-6 mb-5">
                            <div class="h4 mb-4">Установка в два счета</div>
                            <p>Для установки и запуска чат-бота не потребуется специальных навыков программирования. Установка занимает
                                несколько секунд, и бот сразу же начинает свою работу, зарабатывая вам деньги.</p>
                        </div>
                    </div>
                    <div class="pb-5 d-none d-lg-block"></div>
                    <div class="row align-items-center">
                        <div class="col-md-5 col-lg-6 text-center mb-4"><img src="<?php bloginfo('template_url'); ?>/images/business-svg.svg" class="img-fluid" width="315" height="321"></div>
                        <div class="col-md-8 col-lg-6 mb-5">
                            <div class="h4 mb-4">Больше, чем просто оператор</div>
                            <p>Наши решения работают 24 часа в сутки, 7 дней в неделю, без перерывов и выходных. Чат-бот стоит дешевле
                                оператора, он не забывает ответить Вашим клиентам и обрабатывает 100% всех чатов. </p>
                        </div>
                    </div>
                    <div class="pb-5 d-none d-lg-block"></div>
                    <div class="row align-items-center">
                        <div class="col-md-5 col-lg-6 text-center mb-4 order-md-last"><img src="<?php bloginfo('template_url'); ?>/images/marketing-strategy-svg.svg" class="img-fluid" width="288" height="320"></div>
                        <div class="col-md-8 col-lg-6 mb-5">
                            <div class="h4 mb-4">Поможет повысить конверсию</div>
                            <p>Если клиент не закончил общение в чате, забыл или отвлекся, наш чат-бот обязательно напомнит ему, и
                                продолжит диалог, деликатно подводя клиента к сделке. Своевременные напоминания могут повысить конверсию
                                до 30%.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pb-5"></div>
        <div class="h1 mb-5 text-center">Наши партнеры</div>
        <div class="row align-items-center">
            <div class="col text-center mb-5 py-lg-5 border-right"><img src="<?php bloginfo('template_url'); ?>/images/500startups.png" width="auto" height="57"></div>
            <div class="col text-center mb-5 py-lg-5 border-right"><img src="<?php bloginfo('template_url'); ?>/images/sber@2x.png" width="237" height="57"></div>
            <div class="col text-center mb-5 py-lg-5"><img src="<?php bloginfo('template_url'); ?>/images/mtsstartuphub.png" width="auto" height="57"></div>
        </div>
        <div class="pb-5"></div>
        <div class="h1 mb-5 text-center">Наши клиенты</div>
        <div class="row align-items-center partners-list">
            <div class="col text-center mb-5 py-lg-5 border-right"><img src="<?php bloginfo('template_url'); ?>/images/phillips.png" style="mix-blend-mode: multiply;" width="154" height="30"></div>
            <div class="col text-center mb-5 py-lg-5 border-right"><img src="<?php bloginfo('template_url'); ?>/images/askona.png" style="mix-blend-mode: multiply;" width="205" height="52"></div>
            <div class="col text-center mb-5 py-lg-5 border-right"><img src="<?php bloginfo('template_url'); ?>/images/perekryostok-logo-2014@2x.png" width="230" height="34"></div>
            <div class="col text-center mb-5 py-lg-5"><img src="<?php bloginfo('template_url'); ?>/images/fxtm.png" style="mix-blend-mode: multiply;" width="154" height="48"></div>
        </div>
        <div class="pb-5"></div>
        <div id="order" class="card shadow-lg mb-5 main-card">
            <div class="card-body p-5">
                <div class="row align-items-center">
                    <div class="col-md-8 col-lg-5">
                        <div class="h1 mb-4">Не нашли бота, которого искали?</div>
                        <p class="text-muted mb-4">Разработаем бота строго под задачи вашего бизнеса. Это позволит в разы увеличить эффективность внедрения решения. Оставьте заявку и мы свяжемся с Вами.</p>
                        <?php echo do_shortcode( '[contact-form-7 id="143" title="Заявка на главной странице"]' ); ?>
                    </div>
                    <div class="col-md-4 col-lg-7 text-center d-none d-lg-block"><img src="<?php bloginfo('template_url'); ?>/images/mobile-notification-svg@2x.png" class="img-fluid" width="500" height="372"></div>
                </div>
            </div>
        </div>
        <div class="pb-5"></div>
    </div>
</div>
<?php include( locate_template('template-part-modal.php') ); ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>