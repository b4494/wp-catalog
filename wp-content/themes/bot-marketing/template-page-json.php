<?php 
/* Template name: JSON Export */

global $wp;
$output = array();

if( !isset( $_GET['id'] ) ) {

    $posts = new WP_Query(array(
        'tax_query' => array(
            array(
                'taxonomy' => 'post_tag',
                'field' => 'slug',
                'terms' => array( 'new', 'old' )
            )
        ),
        'posts_per_page' => -1
    ));

    if( $posts->have_posts() ) {
        while( $posts->have_posts() ) {
            $posts->the_post();

            $category = get_the_terms( get_the_ID(), 'category' );

            $output[] = array(
                'title' => get_the_title( get_the_ID() ),
                'category' => $category[0]->name,
                'url' => get_the_permalink( get_the_ID() ),
                'json-link' => home_url( $wp->request ) . '?id=' . get_the_ID()
            );
        }
        wp_reset_postdata();

    }

} else {

    $category = get_the_terms( $_GET['id'], 'category' );

    $output[] = array(
        'title' => get_the_title( $_GET['id'] ),
        'category' => $category[0]->name,
        'url' => get_the_permalink( $_GET['id'] ),
        'content' => get_the_content( null, false, $_GET['id'] )
    );
    
}

header( "Content-Type: application/json" );
echo json_encode($output); 

exit;