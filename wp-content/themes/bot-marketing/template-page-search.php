<?php 
/* Template name: Страница поиска */
get_header(); 
?>
<div class="main-content py-3">
    <div class="container-fluid">
        <div class="row">
            <?php get_sidebar('wiki'); ?>
            <div class="col-lg-9">
                <div class="search_entry white_plate">
                    <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                    ?>
                    <p class="seachform">
		    <form id="wiki_searchform" method="get" action="<?php echo home_url('/wiki-search'); ?>">
                            <input value="<?php echo $_GET['search']; ?>" type="text" name="search" class="" placeholder="Ключевое слово, например: «Ярлык» или «Рассылка»">
                            <input type="submit" value="<?php esc_attr_e('Search'); ?>" class="button">
                        </form>
                    </p>
                </div>
                <div class="white_plate">
                    <div class="category_area">
                    <h2>Статьи, найденные по запросу "<?php echo $_GET['search']; ?>"</h2>
                    <p class="desc"></p>
                    <?php 

                    $search = new WP_Query(array(
                        'post_type' => 'post',
                        's' => $_GET['search']
                    ));
                    if ( $search->have_posts() ) {
                        echo '<div class="info_plates">';
                        while ( $search->have_posts() ) {
                            $search->the_post(); 
                            printf( 
                                '<a class="info_plate" href="%s"><div class="title">%s</div><div class="link">Подробнее</div></a>',
                                get_the_permalink( get_the_ID() ),
                                get_the_title( get_the_ID() )
                            );
                        } 
                        wp_reset_postdata();
                        echo '<div class="clearfix"></div></div>';
                    } else {
                        print('<p>Статьи по указанному запросу не найдены. Пожалуйста, воспользуйтесь меню.</p>');
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
