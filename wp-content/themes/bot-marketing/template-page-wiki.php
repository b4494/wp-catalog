<?php 
/* Template name: База знаний */
get_header(); 
?>
<div class="main-content py-3">
    <div class="container-fluid">
        <div class="row">
            <?php get_sidebar('wiki'); ?>
            <div class="col-lg-9">
                <div class="search_entry white_plate">
                    <h1 class="main_heading">База знаний</h1>
                    <p class="desc p24">Ответы на часто задаваемые вопросы, гид по функциям, пошаговое обучение работе с конструктором и полезные мелочи.</p>
                    <p class="seachform">
		    <form id="wiki_searchform" method="get" action="<?php echo site_url(); ?>/wiki-search">
                            <input value="" type="text" name="search" class="" placeholder="Ключевое слово, например: «Ярлык» или «Рассылка»">
                            <input type="submit" value="<?php esc_attr_e('Search'); ?>" class="button">
                        </form>
                    </p>
                </div>
                
                <div class="white_plate">
                    <?php 
                        $categories = get_terms(array( 
                            'taxonomy' => 'category',
                            'hide_empty' => true,
                            'orderby' => 'term_id'
                        ));

                        foreach( $categories as $category ) {
                            echo '<div class="category_area">';
                            printf(
                                '<h2>%s</h2><p class="desc">%s</p>',
                                $category->name,
                                $category->description
                            );

                            $posts = new WP_Query(array(
                                'cat' => $category->term_id,
                                'order' => 'ASC',
                                'posts_per_page' => -1
                            ));

                            if( $posts->have_posts() ) {
                                echo '<div class="info_plates">';
                                while( $posts->have_posts() ) {
                                    $posts->the_post();
                                    printf(
                                        '<a class="info_plate" href="%s"><div class="title">%s</div><div class="link">Подробнее</div></a>',
                                        get_the_permalink( get_the_ID() ),
                                        get_the_title( get_the_ID() )
                                    );
                                }
                                wp_reset_postdata();
                                echo '<div class="clearfix"></div></div>';
                            }

                            echo '</div><div class="clearfix"></div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer('wiki'); ?>
