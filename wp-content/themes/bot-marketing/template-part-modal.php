<div class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content" id="demo_solution">
      <div class="modal-header">
        <h5 class="modal-title">Выберите мессенджер для живого демо</h5>
      </div>
      <div class="modal-body">
        <p>Выберите мессенджер, который установлен на вашем устройстве.</p>
        <div class="social-buttons">
          <a
            target="_blank"
            href="https://api.whatsapp.com/send?phone=79066796730&text=Хочу демо"
            class="btn btn-outline-social social-button whatsapp"
            id="whatsapp"
            onclick="ym(48120641, 'reachGoal', 'whatsapp'); bmw('<?php echo WIDGET_CODE; ?>','click', this.href, 'whatsapp', {
              id: this.dataset.hash,
              type: 'demo',
              integration: '<?php echo INTEGRATION; ?>'
              }, true); return false;"
          >WhatsApp</a>

          <a
            target="_blank"
            href="https://t.me/marketing_support_bot?start={code}"
            class="btn btn-outline-social social-button telegram"
            id="telegram"
            onclick="ym(48120641, 'reachGoal', 'telegram');  bmw('<?php echo WIDGET_CODE; ?>','click', this.href, 'telegram', {
              id: this.dataset.hash,
              type: 'demo',
              integration: '<?php echo INTEGRATION; ?>',
              }, true); return false;"
          >Telegram</a>
        </div>
      </div>
    </div>
  </div>
</div>
