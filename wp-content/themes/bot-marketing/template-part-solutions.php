<?php $local_solution = get_page_by_title( $item['id'], OBJECT, 'solution' ); ?>
<div class="col-lg-4 col-vw-40 col-sm-12 col-md-8 col-12 mb-3" style="min-width: 21rem; max-width: 100%; height: 23rem;">
    <div class="solution-card position-relative text-center">
        <div class="front position-absolute bg-white p-3 border-1 border-light shadow rounded" style="width: 100%; height: 100%;">
           
                  
                       <h5 class="card-title mb-3 mt-5">
                        <a href="<?php echo home_url('/solution/'.ctl_sanitize_title( mb_strtolower( $item['title'] ) ).'_'.$item['id']); ?>" class="stretched-link text-decoration-none text-dark">
                          <?php echo $item['title']; ?>
                        </a>
                      </h5>


				  <div class="tags mt-5">
                    <?php foreach( $item['tags'] as $tag ) { ?>
                        <small class="text-muted">
			    <a class="fs-6 text-secondary text-decoration-none" href="<?php echo home_url('/tagline/'.ctl_sanitize_title( mb_strtolower( $tag['lb'] ) ).'_'.$tag['id']); ?>">
                                <?php echo $tag['lb']; ?>
                            </a>
                        </small>
                    <?php } ?>
                    </div>

                      <div class="position-absolute bottom-0 start-50 translate-middle">
			                <a href="<?php echo home_url('/solution/'.ctl_sanitize_title( mb_strtolower( $item['title'] ) ).'_'.$item['id']); ?>"
                        class="btn btn-outline-danger mb-3">Подробнее  <svg width="26" height="14" viewBox="0 -3 14 14" fill="none" xmlns="http://www.w3.org/2000/svg" class="fm-icon fm-ml5 mb-2"><path d="M1.872 6.525h9.037l-2.38-2.38a.483.483 0 0 1 0-.7.483.483 0 0 1 .7 0l3.213 3.22c.052.04.1.088.14.14.049.121.049.257 0 .378a.793.793 0 0 1-.105.161l-3.22 3.213a.49.49 0 0 1-.35.147.47.47 0 0 1-.343-.147.483.483 0 0 1 0-.7l2.345-2.352H1.872a.49.49 0 1 1 0-.98z" fill="#fe1b51"></path></svg></a>

                        <?php if (isset($item['demo']) && array_filter($item['demo'])) { ?>
                        <a href="javascript:;" class="btn btn-demo"
                            data-name="<?php echo $item['title']; ?>"
                            data-hash="<?php echo $item['hash']; ?>"
                            data-whatsapp="<?php if( isset( $item['demo']['whatsapp'] ) ) echo $item['demo']['whatsapp']; ?>"
                            data-telegram="<?php if( isset( $item['demo']['telegram'] ) ) echo $item['demo']['telegram']; ?>"
                            data-viber="<?php if( isset( $item['demo']['viber_public'] ) ) echo $item['demo_viber_public']; ?>"
                        >Смотреть демо</a>
                        <?php } ?>
                    </div>

      
         
        </div>
	

	
				<div class="back position-absolute bg-light p-3 border-1 border-light shadow rounded" style="width: 100%; height: 100%;">
			

               
                      <h5 class="card-title fs-5 mb-3 mt-3 ">
                        <a href="<?php echo home_url('/solution/'.ctl_sanitize_title( mb_strtolower( $item['title'] ) ).'_'.$item['id']); ?>" class="stretched-link text-decoration-none text-dark">
                          <?php echo $item['title']; ?>
                        </a>
                      </h5>

                      <p class="card-text text-dark"><?php echo $item['short_description']; ?></p>
                 



					<div class="tags">
                    <?php foreach( $item['tags'] as $tag ) { ?>
                        <small class="text-muted">
			    <a class="fs-6 text-secondary text-decoration-none" href="<?php echo home_url('/tagline/'.ctl_sanitize_title( mb_strtolower( $tag['lb'] ) ).'_'.$tag['id']); ?>">
                                <?php echo $tag['lb']; ?>
                            </a>
                        </small>
                    <?php } ?>
                    </div>


                    <div class="position-absolute bottom-0 start-50 translate-middle">
			                <a
                        href="<?php echo home_url('/solution/'.ctl_sanitize_title( mb_strtolower( $item['title'] ) ).'_'.$item['id']); ?>"
                        class="btn btn-outline-danger mb-3"
                      >Подробнее   <svg width="26" height="14" viewBox="0 -3 14 14" fill="none" xmlns="http://www.w3.org/2000/svg" class="fm-icon fm-ml5 mb-2"><path d="M1.872 6.525h9.037l-2.38-2.38a.483.483 0 0 1 0-.7.483.483 0 0 1 .7 0l3.213 3.22c.052.04.1.088.14.14.049.121.049.257 0 .378a.793.793 0 0 1-.105.161l-3.22 3.213a.49.49 0 0 1-.35.147.47.47 0 0 1-.343-.147.483.483 0 0 1 0-.7l2.345-2.352H1.872a.49.49 0 1 1 0-.98z" fill="#fe1b51"></path></svg></a>

                        <?php if (isset($item['demo']) && array_filter($item['demo'])) { ?>
                        <a href="javascript:;" class="btn btn-demo"
                            data-name="<?php echo $item['title']; ?>"
                            data-hash="<?php echo $item['hash']; ?>"
                            data-whatsapp="<?php if( isset( $item['demo']['whatsapp'] ) ) echo $item['demo']['whatsapp']; ?>"
                            data-telegram="<?php if( isset( $item['demo']['telegram'] ) ) echo $item['demo']['telegram']; ?>"
                            data-viber="<?php if( isset( $item['demo']['viber_public'] ) ) echo $item['demo_viber_public']; ?>"
                        >Смотреть демо</a>
                        <?php } ?>
                    </div>
        </div>
    </div>
</div>
