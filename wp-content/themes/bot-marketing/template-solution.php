<?php
/* Template name: Решение для генерации */
$solution_id = get_query_var('solution_id');
$solution_data = get_solution_by_id($solution_id);
$local_solution = get_page_by_title($solution_id, OBJECT, 'solution');

if ($local_solution) {
    // Вносим изменения в SEO: title, description...
    add_filter('wpseo_title', 'filter_wpseo_title');
    function filter_wpseo_title($title)
    {
        global $solution_data, $local_solution;
        if (!empty($seo_title = get_post_meta($local_solution->ID, '_yoast_wpseo_title', true))) {
            $title = $seo_title;
        } else {
            $title = $solution_data['title'];
        }
        return $title;
    }

    add_filter('wpseo_metadesc', 'filter_wpseo_description');
    function filter_wpseo_description($meta_description)
    {
        global $solution_data, $local_solution;
        if (!empty($seo_description = get_post_meta($local_solution->ID, '_yoast_wpseo_metadesc', true))) {
            $meta_description = $seo_description;
        } else {
            $meta_description = $solution_data['description'];
        }
        return $meta_description;
    }
}

get_header('catalog');
?>
<div class="main-content py-3">
    <div class="main-container">
        <div class="jumbotron shadow-lg p-5 mb-5 bg-white rounded">
            <div class="row">
                <div class="col-lg-7 overflow-hidden pb-1">
                    <div class="d-flex flex-column h-100">
                        <h1 class="mb-5"><?php echo $solution_data['title']; ?></h1>
                        <div class="row">
                            <div class="col mb-5">
                                <div class="text-muted mb-2">Метки</div>
                                <div class="h4 font-weight-normal text-truncate">
                                    <?php foreach ($solution_data['tags'] as $tag) { ?>
					<a href="<?php echo home_url('/tagline/'.ctl_sanitize_title(mb_strtolower($tag['lb'])) . '_' . $tag['id']); ?>"><?php echo $tag['lb']; ?></a>
                                        <?php if (end($solution_data['tags']) != $tag) echo ', '; ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col mb-5">
                                <div class="text-muted mb-2">Стоимость</div>
                                <div class="h4 font-weight-normal">от <?php echo $solution_data['product']['price']; ?> руб.</div>
                            </div>
                        </div>
                        <div class="font-weight-normal mb-5 h5"><?php echo $solution_data['short_description'] ?></div>
                        <div class="mt-auto">
                            <a data-fancybox data-src="#order_solution" href="javascript:;" class="btn btn-primary btn-lg px-sm-5 d-block d-sm-inline-block btn-solution-order">Заказать</a>
                            <?php if (isset($solution_data['demo']) && $solution_data['demo']) { ?>
                                <a data-fancybox data-src="#demo_solution" href="javascript:;" class="btn btn-link text-muted btn-lg px-sm-5 d-block d-sm-inline btn-solution-demo">Живое демо</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 d-none d-lg-flex justify-content-center">
                    <div class="solution-demo-img">
                    <?php if( $local_solution && has_post_thumbnail( $local_solution->ID ) ) { ?>
			<img src="<?php echo get_the_post_thumbnail_url( $local_solution->ID, 'full' ); ?>">
		    <?php } elseif (!empty($solution_data['story_url'])) { ?>
		    <amp-story-player style="width:213px;height:378px;z-index:2"><a href="<?php echo $solution_data['story_url']; ?>"><img
		    src="https://mssg.su/storage/skrinsot-22-10-2021-083645-Sux.png"
      loading="lazy"
      width="100%"
      height="100%"
      amp-story-player-poster-img
    /></a></amp-story-player>
                    <?php } elseif( !empty( $solution_data['demo_image'] ) ) { ?>
                        <img src="<?php echo $solution_data['demo_image']; ?>" alt="<?php echo $solution_data['title']; ?>">
                    <?php } else { ?>
			<img src="https://mssg.su/storage/skrinsot-22-10-2021-083645-Sux.png" alt="<?php echo $solution_data['title']; ?>">
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 mb-lg-5">
                <div class="card mb-3 shadow-lg">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a id="description-tab" href="#description" data-toggle="tab" class="nav-link active">Подробное описание</a></li>
                            <?php if( !empty($solution_data['features']) ) { ?>
                            <li class="nav-item"><a id="features-tab" href="#features" data-toggle="tab" class="nav-link">Особенности решения</a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content mt-3">
                            <div id="description" class="tab-pane show active">
                                <?php echo $solution_data['description']; ?>
                            </div>
                            <?php if( !empty($solution_data['features']) ) { ?>
                            <div id="features" class="tab-pane">
                                <?php echo $solution_data['features']; ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-lg-5">
                <div class="card mb-3 shadow-lg">
                    <div class="card-body tarif_card">
                        <div class="title"><?php echo $solution_data['title']; ?></div>
                        <hr>
                        <?php if( isset( $solution_data['product']['price'] ) ) { ?>
                        <div class="tarif_title">Цена</div>
                        <div class="tarif_value">от <?php echo $solution_data['product']['price']; ?> руб.</div>
                        <hr>
                        <?php } ?>
                        <a data-fancybox data-src="#order_solution" href="javascript:;" class="btn btn-primary btn-lg px-sm-5 d-block d-sm-inline-block btn-solution-install">Заказать</a>
                        <?php if( isset( $solution_data['demo'] ) && $solution_data['demo'] ) { ?>
                            <a data-fancybox data-src="#demo_solution" href="javascript:;" class="demo_link">Посмотреть демо</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($local_solution) { ?>
            <div class="row">
                <div class="col-lg-12 mb-lg-5">
                    <div class="card mb-3 shadow-lg">
                        <div class="card-body content-area">
                            <?php echo do_shortcode($local_solution->post_content); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ( $local_solution && $solution_plans_titles = get_field('solution_plans_titles', $local_solution->ID)) { ?>
            <!--<div class="row">
                <div class="col-lg-12 mb-lg-5">
                    <div class="card mb-3 shadow-lg">
                        <div class="card-body plans_area">
                            <div class="title">Тарифы</div>
                            <table class="table plans">
                                <tbody>
                                    <?php foreach ($solution_plans_titles as $row) { ?>
                                        <tr class="plan">
                                            <?php foreach ($row['cols'] as $col) { ?>
                                                <td>
                                                    <div class="bordered"><?php echo $col['text'] ?></div>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="spacer">
                                            <td></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php if ($solution_plans_titles = get_field('solution_plans_notice', $local_solution->ID)) { ?>
                                <div><?php echo $solution_plans_titles; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>-->
        <?php } ?>
    </div>
</div>
</div>

<div class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content" id="order_solution">
      <div class="modal-header">
        <h5 class="modal-title">Выберите мессенджер, чтобы заказать</h5>
      </div>
      <div class="modal-body">
        <p>Выберите мессенджер, который установлен на вашем устройстве.</p>
        <div class="social-buttons">
          <a
            target="_blank"
            href="https://api.whatsapp.com/send?phone=79066796730&text=Хочу заказать <?php echo $solution_data['title']; ?> ({code})"
            class="btn btn-outline-social social-button whatsapp"
            onclick="ym(48120641, 'reachGoal', 'whatsapp'); bmw('<?php echo WIDGET_CODE; ?>','click', this.href, 'whatsapp', { id: <?php echo $solution_data['id']; ?>, type: 'order' }, true); return false;"
          >WhatsApp</a>
          <a
            target="_blank"
            href="https://t.me/marketing_support_bot?start={code}"
            class="btn btn-outline-social social-button telegram"
            onclick="ym(48120641, 'reachGoal', 'telegram');  bmw('<?php echo WIDGET_CODE; ?>','click', this.href, 'telegram', { id: <?php echo $solution_data['id']; ?>, type: 'order' }, true); return false;"
          >Telegram</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content" id="demo_solution">
      <div class="modal-header">
        <h5 class="modal-title">Выберите мессенджер для живого демо</h5>
      </div>
      <div class="modal-body">
        <p>Выберите мессенджер, который установлен на вашем устройстве.</p>
        <div class="social-buttons">
          <?php if( isset( $solution_data['demo']['whatsapp'] ) ): ?>
            <a
              target="_blank"
              href="https://api.whatsapp.com/send?phone=79066796730&text=Хочу демо <?php echo $solution_data['title']; ?> ({code})"
              class="btn btn-outline-social social-button whatsapp"
              onclick="ym(48120641, 'reachGoal', 'whatsapp'); bmw('<?php echo WIDGET_CODE; ?>','click', this.href, 'whatsapp', {
                id: <?php echo $solution_data['id']; ?>,
                type: 'demo',
                integration: '<?php echo INTEGRATION; ?>'
              }, true); return false;"
            >WhatsApp</a
          <?php endif; ?>

          <?php if( isset( $solution_data['demo']['telegram'] ) ): ?>
            <a
              target="_blank"
              href="https://t.me/marketing_support_bot?start={code}"
              class="btn btn-outline-social social-button telegram"
              onclick="ym(48120641, 'reachGoal', 'telegram');  bmw('<?php echo WIDGET_CODE; ?>','click', this.href, 'telegram', {
                id: <?php echo $solution_data['id']; ?>,
                type: 'demo',
                integration: '<?php echo INTEGRATION; ?>'
                }, true); return false;"
            >Telegram</a>
          <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
