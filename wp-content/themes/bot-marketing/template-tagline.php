<?php 
/* Template name: Генерация по тегам */
$tags = get_taglist();
$tag_id = get_query_var('tag_id');
$local_tagline = get_page_by_title( $tag_id, OBJECT, 'tagline' );

foreach ($tags as $tag) {
	if ($tag['id'] == $tag_id) break;
}

if( $local_tagline ) {
    // Вносим изменения в SEO: title, description...
    add_filter('wpseo_title', 'filter_wpseo_title');
    function filter_wpseo_title($title) {
        global $local_tagline;
        if( !empty( $seo_title = get_post_meta($local_tagline->ID, '_yoast_wpseo_title', true) ) ) {
            $title = $seo_title;
        } 
        return $title;
    }

    add_filter('wpseo_metadesc', 'filter_wpseo_description');
    function filter_wpseo_description($meta_description) {
        global $local_tagline;
        if( !empty( $seo_description = get_post_meta($local_tagline->ID, '_yoast_wpseo_metadesc', true) ) ) {
            $meta_description = $seo_description;
        } 
        return $meta_description;
    }
}

get_header('catalog'); 
?>
<div class="main-content py-3">

    <?php if( $local_tagline ): ?>
    <div class="white-wrap mt-n3 pt-5">
        <div class="container main-container">
            <div class="row align-items-center">
                <div class="col-md-8 col-lg-5 mb-4">
                    <div class="h1 mb-4"><?php the_field('tagline_title', $local_tagline->ID); ?></div>
                    <p class="text-muted mb-4"><?php echo $local_tagline->post_content; ?></p>
                    <a href="#solutions" class="btn px-5 btn-primary btn-lg">Перейти к решениям</a>
                </div>
                <div class="col-md-4 col-lg-7 text-center mb-4">
                    <img src="<?php echo get_the_post_thumbnail_url($local_tagline->ID, 'large'); ?>" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="pb-5"></div>
    </div>
	<?php elseif (isset($tag)): ?>
	<h2><?php echo $tag['lb']; ?></h2>
    <?php endif; ?>
        <div class="solution-list-container" id="solutions">
            <div class="row">
                <div class="col-lg-2">
                    <div class="card mb-3 shadow sticky-top tags-menu">
                        <div class="card-body pb-0">
                            <?php 
                                // отбираем и раскладываем теги по категориям
                                $business = $integration = $messenger = $other = array();
                                #$tags = get_taglist(); 
                                foreach( $tags as $tag ) {
                                    switch ( $tag['ct'] ) {
                                        case 'business':
                                            array_push( $business, $tag );
                                            break;
                                        case 'integration':
                                            array_push( $integration, $tag );
                                            break;
                                        case 'messenger':
                                            array_push( $messenger, $tag );
                                            break;
                                        default:
                                            array_push( $other, $tag );
                                    }
                                }
                            ?>
                            <div class="h6 mb-2">Сфера бизнеса</div>
                            <div class="mb-3">
                                <?php foreach( $business as $business_tag ) {
                                        printf(
                                            '<a href="'.home_url('/tagline/%s').'">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $business_tag['lb'] ) ).'_'.$business_tag['id'],
                                            $business_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Интеграции</div>
                            <div class="mb-3">
                                <?php foreach( $integration as $integration_tag ) {
                                        printf(
                                            '<a href="'.home_url('/tagline/%s').'">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $integration_tag['lb'] ) ).'_'.$integration_tag['id'],
                                            $integration_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Мессенджеры</div>
                            <div class="mb-3">
                                <?php foreach( $messenger as $messenger_tag ) {
                                        printf(
                                            '<a href="'.home_url('/tagline/%s').'">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $messenger_tag['lb'] ) ).'_'.$messenger_tag['id'],
                                            $messenger_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                            <div class="h6 mb-2">Метки</div>
                            <div class="mb-3">
                                <?php foreach( $other as $other_tag ) {
                                        printf(
                                            '<a href="'.home_url('/tagline/%s').'">%s</a><br>',
                                            ctl_sanitize_title( mb_strtolower( $other_tag['lb'] ) ).'_'.$other_tag['id'],
                                            $other_tag['lb']
                                        );
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="row solution-list">
                    <?php 
                        $currentPage = get_query_var( 'pagination' );
                        $tagline_solution_count = 6;
                        if( $local_tagline ) {
                            $tagline_solution_count = get_field('tagline_solution_count', $local_tagline->ID);
                        }
                        $tagline_data = get_tagline_by_id( $tag_id, $tagline_solution_count, $currentPage );
                        foreach( $tagline_data['items'] as $item )  {
                            include( locate_template('template-part-solutions.php') ); 
                        } 
?>
                    </div>
                    <nav>
                        <div class="pagination mb-0 justify-content-center">
                        <?php 
                            echo paginate_links( array(
                                'base'         => @add_query_arg('pagination','%#%#solutions'),
                                'format'       => '?pagination=%#%',
                                'total'        => $tagline_data['totalPages'],
                                'current'      => max( 1, $tagline_data['currentPage'] ),
                                'show_all'     => true,
                                'end_size'     => 1,
                                'mid_size'     => 2,
                                'prev_next'    => false,
                                'prev_text'    => __('«'),
                                'next_text'    => __('»'),
                                'type'         => 'plain',
                                'add_args'     => false,
                                'add_fragment' => '',
                                'before_page_number' => '',
                                'after_page_number'  => ''
                            ));
                        ?>
                        </div>
                    </nav>
                </div>
            </div>
    </div>
</div>
<?php include( locate_template('template-part-modal.php') ); ?>
<?php get_footer(); ?>
