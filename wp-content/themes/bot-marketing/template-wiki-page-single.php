<?php
/* Template name: Страница со стилями базы знаний */
get_header(); 
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="main-content py-3">
    <div class="container-fluid">
        <div class="row">
            <?php get_sidebar('wiki'); ?>
            <div class="col-lg-9">
                <div class="post_content white_plate">
                    <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                    ?>
                    <h1 class="post_heading"><?php the_title(); ?></h1>
                    <?php echo apply_filters( 'the_content', do_shortcode( get_the_content( get_the_ID() ) ) ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer('wiki'); ?>
