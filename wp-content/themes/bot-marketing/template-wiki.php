<?php 
/* Template name: Wiki */ 
get_header(); 
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="main-content py-3">
    <div class="py-5">
        <div class="container-fluid">
            <div class="row mb-xl-3">
                <div class="col mr-auto">
                    <h2><?php the_title(); ?></h2>
                </div>
            </div>
            <div class="row">
                <?php 
                    $categories = get_terms(array(
                        'parent' => 0, 
                        'hide_empty' => 0, 
                        'hierarchical' => 1, 
                        'taxonomy' => 'category', 
                        'pad_counts' => false
                    ));

                    foreach ($categories as $category) {
                ?>
                <div class="col-sm-4">
                    <div class="card wiki_card">
                        <div class="h6 mb-2">
                            <a href="<?php echo get_category_link( $category->term_id ); ?>">
                                <?php echo $category->name; ?>
                            </a>
                        </div>
                        <div class="subcategory mb-3">
                        <?php 
                            $sub_categories = get_terms(array(
                                'parent' => $category->term_id, 
                                'hide_empty' => 0, 
                                'hierarchical' => 1, 
                                'taxonomy' => 'category', 
                                'pad_counts' => false
                            ));
                            foreach( $sub_categories as $sub_category ) {
                                    printf(
                                        '<a href="%s" class="">%s</a><br>',
                                        get_category_link( $sub_category->term_id ),
                                        $sub_category->name
                                    );

                                    $subcat_post = new WP_Query(array(
                                        'posts_per_page' => 100,
                                        'tax_query' => array(
                                            'relation' => 'AND',
                                            array(
                                                'taxonomy' => 'category',
                                                'field' => 'id',
                                                'terms' => array($sub_category->term_id),
                                            ),
                                        )
                                    ));
                                    if ($subcat_post->have_posts()) : ?>
                                        <div class="wiki_list">
                                            <ul>
                                                <?php while ($subcat_post->have_posts()) : $subcat_post->the_post(); ?>
                                                    <li><i class="fa fa-file-text-o"></i><a href="<?php the_permalink(); ?>">
                                                            <?php the_title(); ?>
                                                    </a></li>
                                                <?php endwhile; ?>
                                            </ul>
                                        </div>
                                    <?php endif; 
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer('wiki'); ?>
